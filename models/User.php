<?php

namespace app\models;

use Yii;
use \yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "User".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $auth_key
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'User';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['username', 'password', 'auth_key'], 'string', 'max' => 255],
			[['username', 'password'], 'required'],
            [['username'], 'unique']			        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
        ];
    }
	 public static function findIdentity($id)
    {
        return static::findOne($id);
    }

	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}

    public static function findIdentityByAccessToken($token, $type = null)
    {
		throw new NotSupportedException('You can only login
							by username/password pair for now.');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

	public function validatePassword($password)
	{
		return $this->isCorrectHash($password, $this->password); 
	}
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}

    public function validateAuthKey($authKey)
    {
         return $this->getAuthKey() === $authKey;
    }

    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->auth_key = Yii::$app->security->generateRandomString(32);

     return $return;
    }
		public static function getUsers()
	{
		$users = ArrayHelper::
			map(self::find()->all(), 'id', 'username');
			//$users[null] = "All Users";
		return $users;						
	}	
}
