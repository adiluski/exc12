<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Post".
 *
 * @property integer $id
 * @property integer $AuthorId
 * @property integer $statusId
 * @property string $title
 * @property string $body
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AuthorId', 'statusId'], 'integer'],
            [['body'], 'string'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'AuthorId' => 'Author ID',
            'statusId' => 'Status ID',
            'title' => 'Title',
            'body' => 'Body',
        ];
    }
}
