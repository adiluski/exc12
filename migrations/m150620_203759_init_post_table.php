<?php

use yii\db\Schema;
use yii\db\Migration;

class m150620_203759_init_post_table extends Migration
{
   public function up()
   {
		$this->createTable(
		'post',
			[
				'id' => 'pk',
				'AuthorId' =>'integer',
				'statusId'=>'integer',
				'title' => 'string',
				'body' => 'text',
			]
		);
    }

    public function down()
    {
        $this->dropTable('post');
        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
